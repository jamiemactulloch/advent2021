const fs = require('fs');

//let input = [199,200,208,210,200,207,240,269,260,263];
fs.readFile('input', function(err,data) {
  if(err) throw err;
  let input = data.toString().split("\n").map(x => parseInt(x));
  let deltas = [];

  let subsets = [];
  let sums = [];
  let i = 0
  for (let i = 0; i < input.length-3; i++) {
    let subset = input.slice(i,i+3);
    subsets.push(subset);
    sums.push(subset.reduce((partial, a) => partial + a, 0));
  }

  //console.log("last sum  = " + sums[1997]);

  for(let i = 0; i < sums.length; i++) {
    if (i != 0) {
      deltas.push(sums[i]-sums[i-1]);
    }
  }

  console.log(deltas);
  console.log("deltas count = " + deltas.length);
  let positiveCount = deltas.filter(x => x > 0).length;

  console.log(positiveCount);
});
