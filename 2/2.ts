import fs = require('fs');

class Position {
  horizontal: number;

  depth: number;

  aim: number;

  constructor(_horizontal:number, _depth:number) {
    this.horizontal = _horizontal;
    this.depth = _depth;
    this.aim = 0;
  }

  parse(command: string, units: number) {
    switch (command) {
      case 'forward':
        this.forward(units);
        break;

      case 'down':
        this.down(units);
        break;

      case 'up':
        this.up(units);
        break;

      default:
        break;
    }
  }

  forward(units: number) {
    this.horizontal += units;
    this.depth += (this.aim * units);
  }

  up(units: number) {
    this.aim -= units;
  }

  down(units: number) {
    this.aim += units;
  }

  answer() {
    return this.horizontal * this.depth;
  }
}
fs.readFile('input', (err, data) => {
  if (err) throw err;
  const input = data
    .toString()
    .split('\n')
    .filter((x) => x !== '')
    .map((x) => x.split(' '));

  const position = new Position(0, 0);

  // input = input.map(subarray => subarray.map((x,y) => x,parseInt(y)));
  input.forEach((element) => {
    position.parse(element[0], parseInt(element[1], 10));
  });

  console.log(position.horizontal);
  console.log(position.depth);
  console.log(position.answer());
});
