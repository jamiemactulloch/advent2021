import fs = require('fs');

function count(binary: number[], value: number) {
  return binary.filter((x:number) => x == value).length;
}

async function getInput(filename:string = "input") {
  //promise:Promise<Buffer>
  let promise:Promise<string[]> = new Promise<string[]>((resolve,reject) => {
    fs.promises.readFile(filename).then((data:Buffer) => {
      const input = data.toString().split('\n').filter((line:string) => line != '');
      resolve(input);
    }).catch((error) => {
      reject(error);
    });
  });
  return promise;
}

function transposeMatrix(matrix: any[]) {
  const transpose = matrix[0].map((_,c) => matrix.map(r => r[c]));
  return transpose;
}

function sumStringArray(values: string[]) {
  const bitArray = values.map((elem:string) => parseInt(elem,2));
  return sumArray(bitArray);
}

function sumArray(values: number[]) {
  const sum = values.reduce((partial:number, value:number) => partial + value, 0);
  return sum;
}

function convertStringArrayToBitMatrix(strings:string[]) {
    const charMatrix = strings.map((string:string) => Array.from(string));
    const bitMatrix = charMatrix.map((chars:string[]) => chars.map((char:string) => parseInt(char,2)));
    return bitMatrix;
}

function mostCommonBitPerColumn(bits: number[][]) {
  const transpose:number[][] = transposeMatrix(bits);

  const result = transpose.map((bits:number[]) => mostCommonBitInArray(bits));

  return result;
}

function mostCommonBitInArray(bits: number[]) {
  const oneCount:number = sumArray(bits);
  if (oneCount >= (bits.length/2))
    return 1;
  else
    return 0;
}

function invertBitArray(bits: number[]) {
  const result:number[] = bits.map(bit => bit ^ 1);
  return result;
}

function wtfDoICallThisFunction(bitMatrix:number[][], invert:boolean = false) {
    let bitString:string = null;
    let allowed:number[][] = bitMatrix;
    let buffer:number[][] = [];
    let bitStringLength = bitMatrix[0].length

    for(let i = 0; i < bitStringLength; i++) {
      console.log(buffer.length);
      const transpose = transposeMatrix(allowed);
      let mostCommonBit = mostCommonBitInArray(transpose[i]);
      if (invert) mostCommonBit = mostCommonBit ^ 1;
      buffer = allowed.filter((bitArray:number[]) => bitArray[i] == mostCommonBit);

      if (buffer.length > 0) {
        allowed = buffer;
      }
      else {
        console.log("got here");
        bitString = allowed[0].map((bit:number) => bit.toString()).join('');
        return bitString;
      }
    } 

    if (!bitString) bitString = allowed[0].map((bit:number) => bit.toString()).join('');

    return bitString;
}

function main() {
  getInput().then((input:string[]) => {

    const bitMatrix:number[][] = convertStringArrayToBitMatrix(input);

    const mostCommonBits:number[] = mostCommonBitPerColumn(bitMatrix);

    const oxygenString:string = wtfDoICallThisFunction(bitMatrix);

    console.log(`Oxygen string = ${oxygenString}`);

    const oxygenValue:number = parseInt(oxygenString,2);

    console.log(`Oxygen value = ${oxygenValue}`);

    const leastCommonBits:number[] = invertBitArray(mostCommonBits);

    const co2String:string = wtfDoICallThisFunction(bitMatrix, true);

    console.log(`CO2 string = ${co2String}`);

    const co2Value:number = parseInt(co2String,2);
    
    console.log(`CO2 value = ${co2Value}`);

    console.log(`Result = ${oxygenValue * co2Value}`);


  }).catch((error) => {
    throw error
  });
}

main()
