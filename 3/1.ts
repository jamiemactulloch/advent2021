import fs = require('fs');

function count(binary: number[], value: number) {
  return binary.filter(x => x == value).length;
}

function transposeMatrix(matrix: any[]) {
  const transpose = matrix[0].map((_,c) => matrix.map(r => r[c]));
  return transpose;
}

function sumArray(values: number[]) {
  const sum = values.reduce((partial, value) => partial + value, 0);
  return sum;
}

fs.readFile('input', (err, input) => {
  if (err) throw err;

  const data = 
  input
    .toString()
    .split('\n')
    .filter(x => x != '')
    .map((el) => Array.from(el).map(e => parseInt(e,2)));

  const transpose = transposeMatrix(data);
  
  const ones = transpose.map(x => sumArray(x));

  const mostCommonBitString = ones.map(x => x >= 500 ? 1 : 0);

  const reverseBitString = mostCommonBitString.map(x => x^1)

  const gamma = parseInt(mostCommonBitString.join(''),2);

  const epsilon = parseInt(reverseBitString.join(''),2);

  console.log(gamma * epsilon);
  
});



